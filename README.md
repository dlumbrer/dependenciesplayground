# Example of dependencies visualization

- Web-page: [`index.html`](./index.html)
- Data: [`dependencies_simple.json`](./dependencies_simple.json)

## Deploy via http-server in localhost

1. Install http-server in global mode

```
npm install -g http-server
```

2. Deploy the scene
```
git clone https://gitlab.com/dlumbrer/dependenciesplayground.git
cd dependenciesplayground
http-server
```


## Deploy via gitlab

1. Commit to this repository
2. Go to [https://dlumbrer.gitlab.io/dependenciesplayground](https://dlumbrer.gitlab.io/dependenciesplayground)
